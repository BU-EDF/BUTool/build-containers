#!/bin/bash

distro=almalinux8
account=dgastler
name=multiarch-butool
base_command=docker


build () {
    $base_command buildx build --build-arg platform=$2 --platform=linux/$3 -t $account/$name-$distro:$1-$2 .
    $base_command push $account/$name-$distro:1.0-$2
}

manifest () {
    manifest=""
    for ((i=2 ; i <=$#;i++))
    do
	manifest+=" --amend $account/$name-$distro:$1-${!i} "
    done
    
    
    
    $base_command manifest create \
		  $account/$name-$distro:manifest-latest \
		  $manifest


    $base_command manifest push $account/$name-$distro:manifest-latest
}


build 1.0 arm64v8 arm64/v8
build 1.0 amd64 amd64

manifest 1.0 arm64v8 amd64


#docker buildx build --build-arg platform=arm64v8 --platform=linux/arm64/v8 -t dgastler/butool-centos7:1.0-arm64v8 .
#docker push dgastler/butool-centos7:1.0-arm64v8
#docker buildx build --build-arg platform=arm32v7 --platform=linux/arm/v7   -t dgastler/butool-centos7:1.0-arm32v7 .
#docker push dgastler/butool-centos7:1.0-arm32v7
#docker buildx build --build-arg platform=amd64   --platform=linux/amd64    -t dgastler/butool-centos7:1.0-amd64 .
#docker push dgastler/butool-centos7:1.0-amd64
